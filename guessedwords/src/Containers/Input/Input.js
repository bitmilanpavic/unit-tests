import React, { Component } from 'react';
import { connect } from 'react-redux';
import { guessWord } from '../../Actions';

export class UnconnectedInput extends Component {
  state = {
    guessedWord: null,
  };
  render() {
    let html = '';

    if (!this.props.success) {
      html = (
        <div data-test='input-component'>
          <input
            type='text'
            data-test='input-box'
            onChange={(event) => {
              this.setState({ guessedWord: event.target.value });
            }}
          />
          <button
            data-test='submit-button'
            onClick={() =>
              this.props.guessWord(
                this.props.secretWord,
                this.state.guessedWord
              )
            }
          >
            submit
          </button>
        </div>
      );
    }

    return html;
  }
}

const mapStateToProps = (state) => {
  return {
    success: state.successReducer.success,
    secretWord: state.guessWordReducer.secretWord,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    guessWord: (secretWord, guessedWord) =>
      dispatch(guessWord(secretWord, guessedWord)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UnconnectedInput);
