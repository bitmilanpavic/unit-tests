import React from 'react';
import { shallow } from 'enzyme';

import { findByTestAttr, storeFactory } from '../../../test/testUtils';
import Input, { UnconnectedInput } from './Input';

const setup = (defaultState) => {
  const store = storeFactory({ successReducer: defaultState });
  const wrapper = shallow(<Input store={store} />)
    .dive()
    .dive();
  return wrapper;
};

describe('render', () => {
  describe('word has not been guessed', () => {
    const wrapper = setup({ success: false });

    test('renders component without error', () => {
      const inputComponent = findByTestAttr(wrapper, 'input-component');
      expect(inputComponent.length).toBe(1);
    });
    test('renders input box', () => {
      const inputBox = findByTestAttr(wrapper, 'input-box');
      expect(inputBox.length).toBe(1);
    });
    test('renders submit button', () => {
      const submitButton = findByTestAttr(wrapper, 'submit-button');
      expect(submitButton.length).toBe(1);
    });
  });

  describe('word has been guessed', () => {
    const wrapper = setup({ success: true });

    test('renders empty component', () => {
      expect(wrapper.text()).toBe('');
    });
    test('does not renders input box', () => {
      const inputBox = findByTestAttr(wrapper, 'input-box');
      expect(inputBox.length).toBe(0);
    });
    test('does not renders submit button', () => {
      const submitButton = findByTestAttr(wrapper, 'submit-button');
      expect(submitButton.length).toBe(0);
    });
  });
});

describe('update state', () => {});

describe('redux props', () => {
  test('has states as props', () => {
    const success = true;
    const wrapper = setup({ success });
    const props = wrapper.instance().props;
    expect(props.success).toBe(true);
    expect(props.secretWord).not.toBe('');
    expect(props.guessWord).toBeInstanceOf(Function);
  });
});

describe(`'guessWord' action creator call`, () => {
  test('calls `guessWord when button is clicked`', () => {
    const guessWordMock = jest.fn();
    const props = {
      guessWord: guessWordMock,
    };
    const wrapper = shallow(<UnconnectedInput {...props} />);
    const submitButton = findByTestAttr(wrapper, 'submit-button');

    submitButton.simulate('click');

    const guessWordCallCount = guessWordMock.mock.calls.length;
    expect(guessWordCallCount).toBe(1);
  });
});
