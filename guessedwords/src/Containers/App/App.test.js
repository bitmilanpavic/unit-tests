import React from 'react';
import { shallow } from 'enzyme';
import App, { UnconnectedApp } from './App';

import { findByTestAttr, storeFactory } from '../../../test/testUtils';

const setup = (state = {}) => {
  const store = storeFactory(state);
  const wrapper = shallow(<App store={store} />).dive();
  return wrapper;
};

test(`'getSecretWord' runs on App mount`, () => {
  const getSecretWordMock = jest.fn();

  // setup app component with getSecretWordMock as the getSecretWord prop
  const wrapper = shallow(<UnconnectedApp getSecretWord={getSecretWordMock} />);

  // run lifecycle method
  // Instance gives us actuall react component
  wrapper.instance().componentDidMount();

  // check to see if mock ran
  const getSecretWordCallCount = getSecretWordMock.mock.calls.length;
  expect(getSecretWordCallCount).toBe(1);
});

const defaultState = {
  success: false,
  message: 'Try to guess secret word!​',
  secretWord: 'party',
  guessedWords: [],
  guessedWord: null,
};

test('renders component', () => {
  // const wrapper = setup();
  // const appComponent = findByTestAttr(wrapper, 'app-component');
  // expect(appComponent.length).toBe(1);
});

describe('test findLetterMatch function', () => {
  // const secretWord = 'party';
  // const fn = new App().findLetterMatch;
  // test('returns 0 matching letters', () => {
  //   expect(fn(secretWord, 'z').length).toBe(0);
  // });
  // test('returns 1 matching letters', () => {
  //   expect(fn(secretWord, 'zy').length).toBe(1);
  // });
  // test('returns 2 matching letters', () => {
  //   expect(fn(secretWord, 'zaaawey').length).toBe(2);
  // });
});
