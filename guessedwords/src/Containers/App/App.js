import React, { Component } from 'react';
import Congrats from '../../Components/Congrats/Congrats';
import GuessedWords from '../../Components/GuessedWords/GuessedWords';
import Input from '../Input/Input';
import { connect } from 'react-redux';
import { getSecretWord } from '../../Actions';

export class UnconnectedApp extends Component {
  componentDidMount() {
    this.props.getSecretWord();
  }
  render() {
    return (
      <div className='App' data-test='app-component'>
        <h1>Jotto</h1>
        <Input />
        <Congrats success={this.props.success} message={this.props.message} />
        <GuessedWords guessedWords={this.props.guessedWords} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    message: state.guessedWordReducer.message,
    secretWord: state.guessedWordReducer.secretWord,
    guessedWords: state.guessedWordReducer.guessedWords,
    guessedWord: state.guessedWordReducer.guessedWord,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSecretWord: () => dispatch(getSecretWord()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UnconnectedApp);
