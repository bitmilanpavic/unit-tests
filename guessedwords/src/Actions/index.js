import axios from 'axios';

export const actionTypes = {
  CORRECT_GUESS: 'CORRECT_GUESS',
  GUESS_WORD: 'GUESS_WORD',
  GET_SECRET_WORD: 'GET_SECRET_WORD',
};

export function getSecretWord() {
  return async (dispatch) => {
    const secretWord = await axios
      .get(`https://jsonplaceholder.typicode.com/users`)
      .then((res) => res.data[0].username);
    console.log(secretWord);
    dispatch({
      type: actionTypes.GET_SECRET_WORD,
      payload: secretWord,
    });
  };
}

export function correctGuess() {
  return { type: actionTypes.CORRECT_GUESS };
}

export function guessWord(guessedWord) {
  const findLetterMatch = (secretWord, guessedWord) => {
    const secretWordLetters = new Set(secretWord.split(''));
    const guessedWordLetters = new Set(guessedWord.split(''));
    return [...secretWordLetters].filter((el) => guessedWordLetters.has(el));
  };

  return (dispatch, getState) => {
    const secretWord = getState().guessedWordReducer.secretWord;
    const lettersMatch = findLetterMatch(secretWord, guessedWord).length;

    dispatch({
      type: actionTypes.GUESS_WORD,
      payload: {
        guessedWord,
        lettersMatch,
      },
    });

    if (guessedWord === secretWord) {
      dispatch({ type: actionTypes.CORRECT_GUESS });
    }
  };
}
