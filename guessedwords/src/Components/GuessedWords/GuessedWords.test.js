import React from 'react';
import { shallow } from 'enzyme';
import GuessedWords from './GuessedWords';

const defaultProps = { guessedWords: [] };

const setup = (props = {}) => {
  const setupProps = { ...defaultProps, ...props };
  const wrapper = shallow(<GuessedWords {...setupProps} />);
  return wrapper;
};

import { findByTestAttr } from '../../../test/testUtils';

test('renders component', () => {
  const wrapper = setup();
  const guessedWordsComponent = findByTestAttr(
    wrapper,
    'guessedWords-component'
  );
  expect(guessedWordsComponent.length).toBe(1);
});

test('renders empty component at start', () => {
  const wrapper = setup();
  const guessedWordsComponent = findByTestAttr(
    wrapper,
    'guessedWords-component'
  );
  expect(guessedWordsComponent.text()).toBe('');
});

test('renders component with words', () => {
  const props = {
    guessedWords: [
      { guessedWord: 'test', lettersMatched: 2 },
      { guessedWord: 'test2', lettersMatched: 2 },
    ],
  };
  const wrapper = setup(props);
  const guessedWordsComponent = findByTestAttr(
    wrapper,
    'guessedWords-component'
  );
  expect(guessedWordsComponent.text()).toContain('test');
  expect(guessedWordsComponent.text()).toContain('test2');
});

// describe('name', ()=>{
//     let wrapper;
//     beforeEach(()=>{
//         wrapper = setup();
//     })
//     test('test', ()=>{

//     })
// });
