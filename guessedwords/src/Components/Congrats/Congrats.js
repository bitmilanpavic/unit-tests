import React from 'react';

const Congrats = (props) => {
  const style = {};

  if (props.success) {
    style.border = '1px solid green';
  }

  return (
    <div className='message' style={style} data-test='congrats-component'>
      {props.message}
    </div>
  );
};

export default Congrats;
