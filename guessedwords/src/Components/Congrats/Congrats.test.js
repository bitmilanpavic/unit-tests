import React from 'react';
import { shallow } from 'enzyme';
import Congrats from './Congrats';

import { findByTestAttr } from '../../../test/testUtils';

const setup = (props = {}) => {
  const wrapper = shallow(<Congrats {...props} />);
  return wrapper;
};

test('renders component', () => {
  const wrapper = setup();
  const congratsComponent = findByTestAttr(wrapper, 'congrats-component');
  expect(congratsComponent.length).toBe(1);
});

test(`renders received message`, () => {
  const props = { success: true, message: 'test message' };
  const wrapper = setup(props);
  const congratsComponent = findByTestAttr(wrapper, 'congrats-component');
  expect(congratsComponent.text()).toBe('test message');
  //expect(congratsComponent.text()).not.toBe('');
});
