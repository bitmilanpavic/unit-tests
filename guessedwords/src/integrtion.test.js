import { storeFactory } from '../test/testUtils';
import { actionTypes, guessWord, getSecretWord } from './Actions';
import moxios from 'moxios';

const initialState = {
  message: 'Try to guess secret word!​',
  secretWord: 'party',
  guessedWords: [],
  guessedWord: null,
};

describe('GUESS_WORD action and state check', () => {
  test('not correct guess', () => {
    const store = storeFactory({
      successReducer: { success: false },
      guessedWordReducer: initialState,
    });

    store.dispatch(guessWord('test'));
    const newState = store.getState().guessedWordReducer;

    const expectedState = {
      ...initialState,
      guessedWords: [{ guessedWord: 'test', lettersMatch: 1 }],
      guessedWord: 'test',
    };
    expect(newState).toEqual(expectedState);
  });

  test('correct guess', () => {
    const store = storeFactory({
      successReducer: { success: false },
      guessedWordReducer: initialState,
    });

    store.dispatch(guessWord('party'));
    const newState = store.getState();

    const expectedState = {
      successReducer: { success: true },
      guessedWordReducer: {
        ...initialState,
        guessedWords: [{ guessedWord: 'party', lettersMatch: 5 }],
        guessedWord: 'party',
      },
    };

    expect(newState).toEqual(expectedState);
  });
});

describe('secret word', () => {
  beforeEach(() => {
    moxios.install();
  });
  afterEach(() => {
    moxios.uninstall();
  });

  test('Test getting secret word', () => {
    const store = storeFactory({
      successReducer: { success: false },
      guessedWordReducer: initialState,
    });
    const secretWord = 'Bret';

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: [{ username: secretWord }],
      });
    });

    return store.dispatch(getSecretWord()).then(() => {
      const newState = store.getState().guessedWordReducer;
      expect(newState.secretWord).toBe(secretWord);
    });
  });
});
