import { actionTypes } from '../Actions';

const initialState = {
  message: 'Try to guess secret word!​',
  secretWord: 'party',
  guessedWords: [],
  guessedWord: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GUESS_WORD:
      return {
        ...state,
        guessedWords: state.guessedWords.concat(action.payload),
        guessedWord: action.payload.guessedWord,
      };
    case actionTypes.GET_SECRET_WORD:
      return {
        ...state,
        secretWord: action.payload,
      };
    default:
      return state;
  }
};
