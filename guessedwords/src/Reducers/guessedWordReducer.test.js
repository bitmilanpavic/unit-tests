import { actionTypes } from '../Actions';
import guessedWordReducer from './guessedWordReducer';

test(`return default initial state for 'guessedWord' and 'guessedWords'`, () => {
  const newState = guessedWordReducer(undefined, {});
  expect(newState.guessedWord).toBe(null);
  expect(newState.guessedWords).toEqual([]);
});
