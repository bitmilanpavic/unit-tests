import { actionTypes } from '../Actions';
import successReducer from './successReducer';

test(`return default initial state of 'false' when no action`, () => {
  const newState = successReducer(undefined, {});
  expect(newState.success).toBe(false);
});

test(`return state of true upon receiving an action of type 'CORRECT_GUESS'`, () => {
  const newState = successReducer(undefined, {
    type: actionTypes.CORRECT_GUESS,
  });
  expect(newState.success).toBe(true);
});
