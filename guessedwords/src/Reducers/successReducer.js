import { actionTypes } from '../Actions';

const initialState = {
  success: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CORRECT_GUESS:
      return { success: true };
    default:
      return state;
  }
};
