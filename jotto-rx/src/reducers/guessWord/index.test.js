import guessWordReducer, {
  initialGuessWordReducerState,
} from '../../reducers/guessWord';
import actionTypes from '../../actions';

describe('guessWord reducer', () => {
  test('test initial state', () => {
    const state = guessWordReducer(initialGuessWordReducerState, {});

    expect(state).toEqual({
      message: 'Try to guess secret word!​',
      secretWord: null,
      guessedWord: null,
      guessedWords: [],
    });
  });

  test(`check state for '${actionTypes.GUESS_WORD}' action`, () => {
    // Run action first time
    const state = guessWordReducer(undefined, {
      type: actionTypes.GUESS_WORD,
      payload: {
        guessedWord: 'test',
        guessedWords: { guessedWord: 'test', lettersMatched: 2 },
      },
    });

    const expectedState = {
      message: 'Try to guess secret word!​',
      secretWord: null,
      guessedWord: 'test',
      guessedWords: [{ guessedWord: 'test', lettersMatched: 2 }],
    };

    expect(state).toEqual(expectedState);

    // Run action second time
    const updatedState = guessWordReducer(expectedState, {
      type: actionTypes.GUESS_WORD,
      payload: {
        guessedWord: 'test2',
        guessedWords: { guessedWord: 'test2', lettersMatched: 3 },
      },
    });

    expect(updatedState).toEqual({
      message: 'Try to guess secret word!​',
      secretWord: null,
      guessedWord: 'test2',
      guessedWords: [
        { guessedWord: 'test', lettersMatched: 2 },
        { guessedWord: 'test2', lettersMatched: 3 },
      ],
    });
  });
});
