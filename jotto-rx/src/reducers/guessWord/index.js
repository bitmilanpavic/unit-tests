import actionTypes from '../../actions';

export const initialGuessWordReducerState = {
  message: 'Try to guess secret word!​',
  secretWord: null,
  guessedWord: null,
  guessedWords: [],
};

export default (state = initialGuessWordReducerState, action) => {
  switch (action.type) {
    case actionTypes.GET_SECRET_WORD:
      return {
        ...state,
        secretWord: action.payload,
      };
    case actionTypes.GUESS_WORD:
      return {
        ...state,
        guessedWord: action.payload.guessedWord,
        guessedWords: state.guessedWords.concat(action.payload.guessedWords),
      };
    case actionTypes.GUESSED_CORRECTLY:
      return {
        ...state,
        message: 'Congratulations you guessed the word!',
      };
    default:
      return state;
  }
};
