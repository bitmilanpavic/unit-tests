import { combineReducers } from 'redux';
import successReducer from './success';
import guessWordReducer from './guessWord';

export default combineReducers({
  successReducer,
  guessWordReducer,
});
