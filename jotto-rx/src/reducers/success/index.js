import actionTypes from '../../actions';

export const initialSuccessReducerState = {
  success: false,
};

export default (state = initialSuccessReducerState, action) => {
  switch (action.type) {
    case actionTypes.GUESSED_CORRECTLY:
      return {
        success: true,
      };
    default:
      return state;
  }
};
