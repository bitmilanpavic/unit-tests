import successReducer, {
  initialSuccessReducerState,
} from '../../reducers/success';
import actionTypes from '../../actions';

describe('success reducer', () => {
  test('initial success state should be `false`', () => {
    const state = successReducer(initialSuccessReducerState, {});
    expect(state.success).toBe(false);
  });

  test(`return success state 'true' if action is ${actionTypes.GUESSED_CORRECTLY}`, () => {
    const state = successReducer(undefined, {
      type: actionTypes.GUESSED_CORRECTLY,
    });
    expect(state.success).toBe(true);
  });
});
