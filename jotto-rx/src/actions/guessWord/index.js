import axios from 'axios';
import actionTypes from '../index';

export const findLetterMatch = (guessedWord, secretWord) => {
  const secretWordLetters = new Set(secretWord.split(''));
  const guessedWordLetters = new Set(guessedWord.split(''));
  return [...secretWordLetters].filter((el) => guessedWordLetters.has(el));
};

export function getSecretWord() {
  return async (dispatch) => {
    const secretWord = await axios
      .get(`https://jsonplaceholder.typicode.com/users`)
      .then((res) => res.data[0].username);

    console.log(secretWord);

    dispatch({
      type: actionTypes.GET_SECRET_WORD,
      payload: secretWord,
    });
  };
}

export const guessWord = (guessedWord, secretWord) => {
  const lettersMatched = findLetterMatch(guessedWord, secretWord).length;

  return (dispatch) => {
    dispatch({
      type: actionTypes.GUESS_WORD,
      payload: {
        guessedWord: guessedWord,
        guessedWords: {
          guessedWord: guessedWord,
          lettersMatched,
        },
      },
    });

    if (guessedWord === secretWord) {
      dispatch({
        type: actionTypes.GUESSED_CORRECTLY,
      });
    }
  };
};
