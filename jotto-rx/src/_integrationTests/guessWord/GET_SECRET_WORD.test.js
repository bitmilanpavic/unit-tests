import moxios from 'moxios';
import actionTypes from '../../actions';
import { setupStore } from '../../../utils';
import { initialSuccessReducerState } from '../../reducers/success';
import { initialGuessWordReducerState } from '../../reducers/guessWord';
import { getSecretWord } from '../../actions/guessWord';

describe(`test ${actionTypes.GET_SECRET_WORD} action`, () => {
  beforeEach(() => {
    moxios.install();
  });
  afterEach(() => {
    moxios.uninstall();
  });

  test('secretWord should not be `null`', () => {
    const store = setupStore({
      successReducer: initialSuccessReducerState,
      guessWordReducer: initialGuessWordReducerState,
    });

    const secretWord = 'Bret';

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: [{ username: secretWord }],
      });
    });

    return store.dispatch(getSecretWord()).then(() => {
      const state = store.getState().guessWordReducer.secretWord;
      expect(state).toBe(secretWord);
    });
  });
});
