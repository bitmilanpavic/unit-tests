import { setupStore } from '../../../utils';
import actionTypes from '../../actions';
import { initialSuccessReducerState } from '../../reducers/success';
import { initialGuessWordReducerState } from '../../reducers/guessWord';
import { guessWord } from '../../actions/guessWord';

describe(`test ${actionTypes.GUESS_WORD} action and reducer`, () => {
  test('test for not correct guess', () => {
    const store = setupStore({
      successReducer: initialSuccessReducerState,
      guessWordReducer: {
        ...initialGuessWordReducerState,
        secretWord: 'party',
      },
    });

    store.dispatch(guessWord('test', 'party'));

    const state = store.getState().guessWordReducer;
    const expectedState = {
      message: 'Try to guess secret word!​',
      secretWord: 'party',
      guessedWord: 'test',
      guessedWords: [
        {
          guessedWord: 'test',
          lettersMatched: 1,
        },
      ],
    };

    expect(state).toEqual(expectedState);
  });

  test('test for correct guess', () => {
    const store = setupStore({
      successReducer: initialSuccessReducerState,
      guessWordReducer: {
        ...initialGuessWordReducerState,
        secretWord: 'party',
      },
    });

    store.dispatch(guessWord('party', 'party'));

    const state = store.getState();
    const expectedState = {
      successReducer: {
        success: true,
      },
      guessWordReducer: {
        message: 'Congratulations you guessed the word!',
        secretWord: 'party',
        guessedWord: 'party',
        guessedWords: [
          {
            guessedWord: 'party',
            lettersMatched: 5,
          },
        ],
      },
    };

    expect(state).toEqual(expectedState);
  });
});
