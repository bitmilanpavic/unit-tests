import React, { Component } from 'react';
import { connect } from 'react-redux';
import { guessWord } from '../../actions/guessWord';

export class UnconnectedInput extends Component {
  state = {
    guessedWord: null,
  };
  render() {
    let html = '';

    if (!this.props.success) {
      html = (
        <div className='row' data-test='input-component'>
          <div className='col-md-6'>
            <input
              className='form-control mb-3'
              type='text'
              data-test='input-box'
              onChange={(event) => {
                this.setState({ guessedWord: event.target.value });
              }}
            />
            <button
              className='btn btn-primary'
              data-test='submit-button'
              onClick={() =>
                this.props.guessWord(
                  this.state.guessedWord,
                  this.props.secretWord
                )
              }
            >
              Submit
            </button>
          </div>
        </div>
      );
    }

    return html;
  }
}

const mapStateToProps = (state) => {
  return {
    success: state.successReducer.success,
    secretWord: state.guessWordReducer.secretWord,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    guessWord: (secretWord, guessedWord) =>
      dispatch(guessWord(secretWord, guessedWord)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UnconnectedInput);
