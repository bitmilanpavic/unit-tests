import React, { Component } from 'react';
import { connect } from 'react-redux';
import GuessedWords from '../../components/guessedWords';
import Message from '../../components/message';
import Input from '../Input';
import { getSecretWord } from '../../actions/guessWord';

export class UnconnectedApp extends Component {
  componentDidMount() {
    this.props.getSecretWord();
  }

  render() {
    return (
      <div className='container' data-test='app-component'>
        <h1>Jotto</h1>
        <Input />
        <Message success={this.props.success} message={this.props.message} />
        <GuessedWords guessedWords={this.props.guessedWords} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    success: state.successReducer.success,
    message: state.guessWordReducer.message,
    guessedWords: state.guessWordReducer.guessedWords,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSecretWord: () => dispatch(getSecretWord()),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(UnconnectedApp);
