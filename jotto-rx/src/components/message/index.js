import React from 'react';

const Message = (props) => {
  let classes = 'mb-4 mt-4';

  if (props.success) {
    classes += ' alert alert-success';
  } else {
    classes += ' alert alert-primary';
  }

  return (
    <div className='row' data-test='messageComponent'>
      <div className='col-md-6'>
        <h3 className={classes}>{props.message}</h3>
      </div>
    </div>
  );
};

export default Message;
