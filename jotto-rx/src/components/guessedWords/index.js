import React from 'react';

const guessedWords = (props) => {
  let html = '';

  if (props.guessedWords.length) {
    html = (
      <div className='row' data-test='guessedWordsComponent'>
        <div className='col-md-6'>
          <ul className='list-group'>
            {props.guessedWords.map((el, index) => {
              return (
                <li className='list-group-item' key={index}>
                  Word: <strong>{el.guessedWord}</strong>, Letters Matched:
                  <strong> {el.lettersMatched}</strong>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }

  return html;
};

export default guessedWords;
