import React from 'react';

const GuessedWords = (props) => {
  let html = '';

  if (props.guessedWords.length) {
    html = (
      <ul>
        {props.guessedWords.map((el) => {
          return (
            <li key={el.guessedWord}>
              {el.guessedWord}, {el.lettersMatched}
            </li>
          );
        })}
      </ul>
    );
  }

  return <div data-test='guessedWords-component'>{html}</div>;
};

export default GuessedWords;
