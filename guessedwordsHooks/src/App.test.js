import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

import { findByTestAttr } from '../test/testUtils';

const setup = (props = {}, state = null) => {
  const wrapper = shallow(<App {...props} />);
  if (state) wrapper.setState(state);
  return wrapper;
};

const defaultState = {
  success: false,
  message: 'Try to guess secret word!​',
  secretWord: 'party',
  guessedWords: [],
  guessedWord: null,
};

test('renders component', () => {
  const wrapper = setup();
  const appComponent = findByTestAttr(wrapper, 'app-component');
  expect(appComponent.length).toBe(1);
});

describe('test findLetterMatch function', () => {
  const secretWord = 'party';
  const fn = new App().findLetterMatch;

  test('returns 0 matching letters', () => {
    expect(fn(secretWord, 'z').length).toBe(0);
  });

  test('returns 1 matching letters', () => {
    expect(fn(secretWord, 'zy').length).toBe(1);
  });

  test('returns 2 matching letters', () => {
    expect(fn(secretWord, 'zaaawey').length).toBe(2);
  });
});
