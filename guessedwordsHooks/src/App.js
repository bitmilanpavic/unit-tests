import React, { Component } from 'react';
import Congrats from './Congrats';
import GuessedWords from './GuessedWords.js';

class App extends Component {
  state = {
    success: false,
    message: 'Try to guess secret word!​',
    secretWord: 'party',
    guessedWords: [],
    guessedWord: null,
  };

  findLetterMatch = (secretWord, guessedWord) => {
    const secretWordLetters = new Set(secretWord.split(''));
    const guessedWordLetters = new Set(guessedWord.split(''));
    return [...secretWordLetters].filter((el) => guessedWordLetters.has(el));
  };

  render() {
    return (
      <div className='App' data-test='app-component'>
        <h1>Jotto</h1>
        <Congrats success={this.state.success} message={this.state.message} />
        <GuessedWords guessedWords={this.state.guessedWords} />
      </div>
    );
  }
}

export default App;
