import React, { Component } from 'react';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0,
    };
  }

  increment = () => {
    const counter = this.state.counter + 1;
    this.setState({ counter });
  };

  render() {
    return (
      <div className='App' data-test='app'>
        <span data-test='app-counter'>Counter is: {this.state.counter}</span>
        <br />
        <button onClick={() => this.increment()} data-test='app-button'>
          increment
        </button>
      </div>
    );
  }
}
