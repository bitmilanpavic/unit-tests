import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import App from './App';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  const wrapper = shallow(<App {...props} />);

  if (state) wrapper.setState(state);

  return wrapper;
};
const findByTestAttr = (wrapper, attr) => wrapper.find(`[data-test='${attr}']`);

test('renders component without error', () => {
  // console.log(wrapper.debug()); expect(wrapper).toBeTruthy();
  const wrapper = setup();
  const appComponent = findByTestAttr(wrapper, 'app');
  expect(appComponent.length).toBe(1);
});

test('renders component button without error', () => {
  const wrapper = setup();
  const appCounter = findByTestAttr(wrapper, 'app-counter');
  expect(appCounter.length).toBe(1);
});

test('renders component counter without error', () => {
  const wrapper = setup();
  const appButton = findByTestAttr(wrapper, 'app-button');
  expect(appButton.length).toBe(1);
});

test('component counter starts from 0', () => {
  const wrapper = setup();
  const initialCounterState = wrapper.state('counter');
  expect(initialCounterState).toBe(0);
});

test('when click on button, it increments counter on dispaly, not testing counter as state', () => {
  // setup wrapper with initial counter state
  const counterVal = 1;
  const wrapper = setup(null, { counter: counterVal });

  // get button and simulate click
  const button = findByTestAttr(wrapper, 'app-button');
  button.simulate('click');

  // get dispaly counter
  const counter = findByTestAttr(wrapper, 'app-counter');
  expect(counter.text()).toContain(counterVal + 1);
});
